import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import { store } from '../store';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Dashboard from '../views/home/Dashboard.vue';
import TicketDetailView from '../views/home/TicketDetailView.vue';
import { accounts } from '@/api';

Vue.use(VueRouter)

export const ROUTE_THREAD_DETAIL = 'thread.detail';
export const ROUTE_TICKET_DETAIL = 'ticket.detail';
export const ROUTE_DASHBOARD = 'home.dashboard';
export const ROUTE_CREATE_SURVEY = 'home.create_survey';
export const ROUTE_API_KEYS = 'settings.api_keys';
export const ROUTE_LOGIN = 'login';
export const ROUTE_SIGNUP = 'signup';


function requireGuest(to: any, from: any, next: any) {
  next(!accounts.isAuthenticated());
}

async function requireUser(to: any, from: any, next: any) {
  // TODO: refine this function

  if (accounts.isAuthenticated()) {
    next(true);
  } else if (to.query.auth_token) {
    try {
      const user = await accounts.login_with_token(to.query.auth_token);
      accounts.setUser(user);
      store.commit('accounts/SET_CURRENT_USER', user);
      next(true);
    } catch (error) {
      console.log(error.response)
      next({
        name: ROUTE_LOGIN,
        query: {
          redirect: to.fullPath,
        },
      });
    }
  }
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    redirect: { name: ROUTE_DASHBOARD },
    beforeEnter: requireUser,
  },
  {
    path: '/home/',
    component: Home,
    beforeEnter: requireUser,
    children: [
      {
        path: 'dashboard',
        name: ROUTE_DASHBOARD,
        component: Dashboard,
      },
      {
        path: 'tickets/:ticketNumber',
        name: ROUTE_TICKET_DETAIL,
        component: TicketDetailView,
      },
    ],
  },
  {
    path: '/account/login',
    name: ROUTE_LOGIN,
    component: Login,
    beforeEnter: requireGuest,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
