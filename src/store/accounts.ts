
import toastr from 'toastr';
import { accounts, WebSocket } from '@/api';
import router, { ROUTE_LOGIN } from '@/router';

const getUser = () => {
  const userStr = localStorage.getItem('current_user');
  return userStr ? JSON.parse(userStr) : null;
};

export default {
  namespaced: true,
  state: {
    currentUser: getUser(),
  },
  mutations: {
    SET_CURRENT_USER: (state: any, user: any) => { state.currentUser = user; },
    DISCONNECT_SOCKET: (state: any, user: any) => { 
      WebSocket.disconnect(); 
    },
  },
  actions: {
    LOGIN: (context: any, { email, password }: any) => {
      const request = accounts.login(email, password);
      request.then(
        (user: any) => {
          accounts.setUser(user);
          context.commit('SET_CURRENT_USER', user);
        },
        () => {
          toastr.error('Login failed. Please check your email and password.');
        },
      );
      return request;
    },
    LOGOUT: (context: any) => {
      const request = accounts.logout().then(
        (data: any) => {
          accounts.deleteUser();
          context.commit('SET_CURRENT_USER', null);
          context.commit('DISCONNECT_SOCKET');
          router.push({ name: ROUTE_LOGIN });
        },
        () => {
          toastr.error('Logout failed. Reload the page and try again.');
        },
      );
      return request;
    },
  },
};
