import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'toastr/build/toastr.css';
import 'jquery';
import 'bootstrap';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCompactDisc,
  faFileUpload,
  faUser,
  faClock,
  faBars,
  faSearch,
  faUserCircle,
  faEnvelope,
  faLock,
  faVolumeOff,
  faVolumeUp,
  faPlay,
  faPause,
  faStepForward,
  faStepBackward,
  faCircleNotch,
  faHome,
  faCog,
  faKey,
  faTrash,
  faLocationArrow,
  faTicketAlt
} from '@fortawesome/free-solid-svg-icons';

library.add(
  faCompactDisc,
  faFileUpload,
  faUser,
  faClock,
  faBars,
  faSearch,
  faUserCircle,
  faEnvelope,
  faLock,
  faVolumeOff,
  faVolumeUp,
  faPlay,
  faPause,
  faStepForward,
  faStepBackward,
  faCircleNotch,
  faHome,
  faCog,
  faKey,
  faTrash,
  faLocationArrow,
  faTicketAlt
);

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
Vue.component('font-awesome-icon', FontAwesomeIcon);


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
