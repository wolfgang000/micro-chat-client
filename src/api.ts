import axios from 'axios';
import toastr from 'toastr';
import { store } from './store';
import router, { ROUTE_LOGIN } from '@/router';
import { Socket } from 'phoenix';
import { IMessage, ITicket } from './models';

const CURRENT_USER_KEY = 'current_user';
const SOCKET_URL = '/api/socket';
export const cleanUserStateAndRedirectToLogin = (context: any) => {
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  accounts.deleteUser();
  context.commit('accounts/SET_CURRENT_USER', null, { root: true });
  context.commit('player/DISCONNECT_SOCKET', null, { root: true });
  router.push({ name: ROUTE_LOGIN });
};

const errorHandler = (error: any) => {
  if (error.response && (error.response.status === 500 || error.response.status > 500)) {
    toastr.error('There was a problem retrieving the data from the server.');
  } else if (error.response && error.response.status === 401) {
    toastr.warning('it seems your session has expired');
    cleanUserStateAndRedirectToLogin(store);
  }
  throw error;
};

export const accounts = {
  getUser() {
    const userStr = localStorage.getItem(CURRENT_USER_KEY);
    return userStr ? JSON.parse(userStr) : null;
  },
  setUser(user: string) {
    localStorage.setItem(CURRENT_USER_KEY, JSON.stringify(user));
  },
  deleteUser() {
    localStorage.removeItem(CURRENT_USER_KEY);
  },
  isAuthenticated() {
    if (this.getUser()) {
      return true;
    }
    return false;
  },
  login(email: string, password: string) {
    return axios
      .post(`/api/tenant/accounts/login`, { user: { email, password } })
      .then((r) => r.data.data)
      .catch(errorHandler);
  },
  login_with_token(token: string) {
    return axios
      .post(`/api/tenant/accounts/login`, { user: { token } })
      .then((r) => r.data.data)
      .catch(errorHandler);
  },
  logout() {
    return axios.post(`/api/tenant/accounts/logout`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getUserToken() {
    return axios.get(`/api/tenant/accounts/get_user_token`)
      .then((r) => r.data.data)
      .catch(errorHandler);
  },
};

export const general = {
  getTicket(ticketNumber: number) {
    return axios
      .get(`/api/tenant/tickets/${ticketNumber}`)
      .then((r) => r.data.data as ITicket)
      .catch(errorHandler);
  },
  getTicketsMessages(ticketNumber: number) {
    return axios
      .get(`/api/tenant/tickets/${ticketNumber}/messages`)
      .then((r) => r.data.data as IMessage[])
      .catch(errorHandler);
  },
};

let socket = new Socket(SOCKET_URL);
let threadChannel = socket.channel('chat');
export const WebSocket = {
  getSocket() {
    return socket;
  },
  getTicketChannel() {
    return threadChannel;
  },
  setTicketChannel(ticketId: number) {
    threadChannel = socket.channel(`ticket:${ticketId}`);
    return threadChannel;
  },
  setToken(token: string) {
    socket = new Socket(SOCKET_URL, {
      params: {token: token},
    });
    threadChannel = socket.channel('chat');
  },
  disconnect() {
    socket.disconnect();
  }
};
